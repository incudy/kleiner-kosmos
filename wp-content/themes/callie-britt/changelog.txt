Version v.1.0.1
     Fixed:
        - The Events Calendar datepicker styles
        - Scripts libraries updated
        - Compatibility with PHP 7.3

Release 1.0